package com.github.cstroe.svndumps

import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.net.URL
import java.nio.file.StandardCopyOption


abstract class SvnBashTask : DefaultTask() {

    @get:Input
    abstract val scriptName: Property<String>

    @get:Input
    abstract val scriptArgs: ListProperty<String>

    @get:OutputDirectory
    abstract val svnWorkDir: DirectoryProperty

    @get:OutputDirectory
    abstract val dumpFileDir: DirectoryProperty

    /**
     * From the svnWorkDir the following tree is constructed.
     *   - checkout
     *   - repository
     *   - scripts
     */
    @TaskAction
    fun exec() {
        // get the scripts
        val utilsMap =
            listOf("setup", "export_v2", "export_v3", "teardown")
                .associateWith { retrieveScript("util/${it}.sh") }

        scriptName.get().let {
            val scriptCacheFile = retrieveScript("bash/${it}.sh") ?: return@let
            val repoFile = svnWorkDir.dir("repository/testrepo").get().asFile.canonicalPath
            val env = mapOf("SVN_REPO" to URL("file", "localhost", repoFile))
            logger.info("env $it $env")
            logger.info("setup.sh $it")
            project.exec {
                executable("/bin/bash")
                args(listOf(utilsMap["setup"]))
                workingDir(svnWorkDir)
                environment(env)
            }
            logger.info("$scriptCacheFile $it")
            project.exec {
                executable("/bin/bash")
                args(if (scriptArgs.isPresent) {
                    listOf(scriptCacheFile.canonicalPath).plus(scriptArgs.get())
                } else {
                    listOf(scriptCacheFile.canonicalPath)
                })
                workingDir(svnWorkDir.dir("checkout"))
                environment(env)
            }

            logger.info("export_v2.sh $it")
            val dumpFileV2 = dumpFileDir.file("v2/${it}.dump").get().asFile
            dumpFileV2.parentFile.mkdirs()
            project.exec {
                executable("/bin/bash")
                args(listOf(utilsMap["export_v2"]))
                workingDir(svnWorkDir)
                standardOutput = dumpFileV2.outputStream()
            }

            logger.info("export_v3 $it")
            val dumpFileV3 = dumpFileDir.file("v3/${it}.dump").get().asFile
            dumpFileV3.parentFile.mkdirs()
            project.exec {
                executable("/bin/bash")
                args(listOf(utilsMap["export_v3"]))
                workingDir(svnWorkDir)
                standardOutput = dumpFileV3.outputStream()
            }

            logger.info("teardown.sh $it")
            project.exec {
                executable("/bin/bash")
                args(listOf(utilsMap["teardown"]))
                workingDir(svnWorkDir)
            }
        }
    }

    private fun retrieveScript(name: String): File? {
        name.let {
            val scriptCacheFile = svnWorkDir.dir("scripts").get().file(it).asFile
            scriptCacheFile.parentFile.mkdirs()

            val depUrl = javaClass.classLoader.getResource(it) ?: return null

            java.nio.file.Files.copy(
                depUrl.openStream(),
                scriptCacheFile.toPath(),
                StandardCopyOption.REPLACE_EXISTING
            )
            return scriptCacheFile
        }
    }
}