#!/bin/bash

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

touch firstFile.txt
svn add firstFile.txt > /dev/null
svn commit -m "Added a first file." > /dev/null
