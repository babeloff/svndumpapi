#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

echo "new content" > README.txt
svn add $SVNFLAGS README.txt
svn commit $SVNFLAGS -m "Added readme."
svn cp $SVNFLAGS README.txt OTHER.txt
svn commit $SVNFLAGS -m "Copied readme."
