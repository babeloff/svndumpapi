#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

mkdir -p dir1/dir2/dir3
svn add $SVNFLAGS dir1
svn commit $SVNFLAGS -m "Adding directories."

echo $1 > dir1/dir2/dir3/README.txt
svn add $SVNFLAGS dir1/dir2/dir3/README.txt
svn commit $SVNFLAGS -m "Committed README.txt"
