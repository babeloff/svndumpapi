#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

mkdir -p trunk/innerdir branches
echo "this is a test file" > trunk/innerdir/README.txt
svn add $SVNFLAGS trunk branches
svn commit $SVNFLAGS -m "Initial commit."
svn copy $SVNFLAGS trunk branches/mybranch
svn commit $SVNFLAGS -m "Creating branch."
cd ..

svn checkout $SVNFLAGS "${SVN_REPO}/branches/mybranch" mybranch

cd mybranch
echo "branch work" >> innerdir/README.txt
svn commit $SVNFLAGS -m "Branch work."
cd ..

svn checkout $SVNFLAGS "${SVN_REPO}/trunk" trunk
cd trunk
svn merge $SVNFLAGS --reintegrate ^/branches/mybranch
svn commit $SVNFLAGS -m "Merge branch back into trunk."
svn delete $SVNFLAGS ^/branches/mybranch -m "Removing branch."
