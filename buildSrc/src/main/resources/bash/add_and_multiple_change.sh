#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

echo "this is a test file" > file1.txt
svn add $SVNFLAGS file1.txt
svn commit -m "Initial commit." $SVNFLAGS

echo "changed file" > file1.txt
svn commit -m "Changed file." $SVNFLAGS

echo "changed file again" > file1.txt
svn commit -m "Changed file again." $SVNFLAGS

echo "changed file a third time" > file1.txt
svn commit -m "Changed file a third time." $SVNFLAGS

