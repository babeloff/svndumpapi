#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

mkdir trunk
mkdir branches
cd trunk
mkdir dir1
cd dir1
echo "this is a test file" > file1.txt
cd ../..
svn add $SVNFLAGS branches trunk
svn commit -m "Initial commit." $SVNFLAGS

svn cp "${SVN_REPO}/trunk" \
  "${SVN_REPO}/branches/branch1" -m "Create branch." \
  $SVNFLAGS

cd "$SCRIPT_DIR" || return
cd checkout/testrepo
svn up $SVNFLAGS
cd trunk/dir1
svn rm $SVNFLAGS file1.txt
svn cp $SVNFLAGS "${SVN_REPO}/branches/branch1/dir1/file1.txt@2" .
svn commit -m "Copy from branch." $SVNFLAGS

echo "changed file" > file1.txt
svn commit -m "Changed file." $SVNFLAGS
