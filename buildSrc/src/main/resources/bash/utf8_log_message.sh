#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

cd checkout/testrepo
echo "this is a test file" > file1.txt
svn add $SVNFLAGS file1.txt
svn commit -m "This commit makes me happy ☺" $SVNFLAGS
