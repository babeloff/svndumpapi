#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

echo "this is a test file" > README.txt
svn add $SVNFLAGS README.txt
svn commit $SVNFLAGS -m "Committed README.txt"
svn mv $SVNFLAGS README.txt README-new.txt
svn commit $SVNFLAGS -m "Renamed README.txt to README-new.txt"

