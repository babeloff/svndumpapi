#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

mkdir -p trunk/innerdir branches/mybranch/innerdir
echo "this is a test file" > trunk/innerdir/README.txt
svn add $SVNFLAGS trunk branches
svn commit $SVNFLAGS -m "Initial commit."
svn copy $SVNFLAGS trunk/innerdir/README.txt branches/mybranch/innerdir/README.txt
svn commit $SVNFLAGS -m "Copying file."

