#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

svn propset $SVNFLAGS customproperty myval .
svn commit $SVNFLAGS -m "Setting property on the root."

