#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

echo "this is a test file" > README.txt
echo "this is a test file" > README2.txt
echo "this is a test file" > README3.txt
svn add $SVNFLAGS README.txt README2.txt README3.txt 
svn commit $SVNFLAGS -m "Added 3 files."
svn rm $SVNFLAGS README.txt README2.txt README3.txt 
svn commit $SVNFLAGS -m "Deleted 3 files."
