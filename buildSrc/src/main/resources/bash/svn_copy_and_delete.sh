#!/bin/bash -e

svn checkout "${SVN_REPO}" testrepo > /dev/null
cd testrepo || return

SVNFLAGS="-q"

FILE_CONTENT=$1

echo $1 > README.txt
svn add $SVNFLAGS README.txt
svn commit $SVNFLAGS -m "Added readme." # r1

svn rm $SVNFLAGS README.txt
svn commit $SVNFLAGS -m "Deleting file." # r2

svn cp $SVNFLAGS README.txt@1 OTHER.txt
svn commit $SVNFLAGS -m "Copied readme from r1." # r3

svn mkdir $SVNFLAGS dir1 
svn cp $SVNFLAGS OTHER.txt dir1/OTHER.txt
svn commit $SVNFLAGS -m "Copied readme again." # r4

svn rm $SVNFLAGS dir1
svn commit $SVNFLAGS -m "Deleting directory." # r5

svn cp $SVNFLAGS dir1@4 otherdir1
svn commit $SVNFLAGS -m "Copied directory from r4." # r6

svn cp $SVNFLAGS otherdir1/OTHER.txt otherdir1/NEWNAME.txt
svn commit $SVNFLAGS -m "Renamed file again." # r7

